import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import Seo from "../components/seo"

import { Button } from 'react-bootstrap'

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <h1>Hi Team AISOL</h1>
    <p>Welcome to your new AISOL-WIKI site.</p>
    <StaticImage
      src="../images/IMG_3965__1_.jpg"
      width={800}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
    />
    <p>
      {/* <Button to="/page-2"> Our team</Button> <br /> <br />
      <Button to="/tool/"> AISOL Tool History </Button> <br /> */}
      <Link className="btn btn-outline-primary" to="/page-2"> Members </Link> <br /> <br />
      <Link className="btn btn-outline-primary" to="/blog/"> Blog </Link> <br /> <br />
      <Link className="btn btn-outline-primary" to="/tool/"> AISOL Tool Development History </Link> <br />

    </p>
    
    {/* <p>
      <Link to="/page-2/">Members</Link> <br />
      <Link to="/tool/">AISOL Tool Development History</Link> <br />
    </p> */}
  </Layout>
)

export default IndexPage
