import * as React from "react"

import Layout from "../../components/layout"
import Seo from "../../components/seo"

const NotFoundPage = () => (
  <Layout>
    <Seo title="AWS Tools" />
    <h1>AWS Tools</h1>
    <dl>
      <dt> [PYTHON][업무 자동화] aviEncoder </dt>
      <dd> 
      - AWS S3에 업로드되는 R8, R9 영상 처리. water mark video, recog video, api call... <br />
      - ssh://git@gitlab.aimatics.synology.me:30002/AWS/aws-aviencoder.git  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [PYTHON][업무 자동화] counter.py </dt>
      <dd> 
      - 특정 시리얼, 날짜 별로 영상 업로드 통계를 엑셀 파일로 작성 <br />
      - Ted WSL : X:\root\work\work-aws\download\video_crawler  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [PYTHON][업무 자동화] counter.py </dt>
      <dd> 
      - 특정 시리얼, 날짜 기준으로 AccidentRisk 영상 다운로드 및 엑셀 파일 작성 <br />
      - Ted WSL : X:\root\work\work-aws\download\video_crawler  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [PYTHON][업무 자동화] crawler_accidentrisk.py </dt>
      <dd> 
      - 특정 시리얼, 날짜 기준으로 AccidentRisk 영상 다운로드 및 엑셀 파일 작성 <br />
      - Ted WSL : X:\root\work\work-aws\download\video_crawler  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [PYTHON][업무 자동화] crawler_tailgating_download.py </dt>
      <dd> 
      - 특정 시리얼, 날짜 기준으로 Tailgating 영상 다운로드 <br />
      - Ted WSL : X:\root\work\work-aws\download\video_crawler  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [PYTHON][업무 자동화] crawler.py </dt>
      <dd> 
      - 특정 시리얼, 날짜 별로 영상 다운로드 및 엑셀 파일 작성 <br />
      - Ted WSL : X:\root\work\work-aws\download\video_crawler  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [PYTHON][업무 자동화] eventlist.py </dt>
      <dd> 
      - 특정 시리얼, 날짜 별로 이벤트 파일 목록 엑셀 파일 작성 <br />
      - Ted WSL : X:\root\work\work-aws\download\video_crawler  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [PYTHON][업무 자동화] safetyevent_condition_reject.py </dt>
      <dd> 
      - 특정 시리얼, 날짜 별로 업로드된 safety event 정보를 보고 기준에 부합하지 않는 것들을 웹페이지에 노출되지 않도록 DB 변경 <br />
      - Ted WSL : X:\root\work\work-aws\download\video_crawler  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [PYTHON][업무 자동화] false_case.py </dt>
      <dd> 
      - 다운로드 및 True/False가 결정된 이벤트 영상을 폴더 별로 분류 <br />
      - Ted WSL : X:\root\work\work-aws\download\video_crawler  <br />
      - 담당자 Ted <br />
      </dd>


    </dl>

  </Layout>
)

export default NotFoundPage
