import * as React from "react"

import Layout from "../../components/layout"
import Seo from "../../components/seo"

const NotFoundPage = () => (
  <Layout>
    <Seo title="R8 Tools" />
    <h1>R8 Tools</h1>
    <dl>
      <dt> [MFC] wVgaMonitoringSocket_GitServer </dt>
      <dd> 
      - R8 모니터링 툴 <br />
      - ssh://gituser@115.178.89.182:58/volume1/homes/gituser/wVgaMonitoringSocket_GitServer <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC] R8_EventAnalyzer </dt>
      <dd> 
      - R8 avi 파일의 메타 데이터로부터 인식 결과 재생 <br />
      - Ted Desktop : D:\Project\2_DSP_CODE\4_NVS3320\_tool\R8_EventAnalyzerWorking <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [Python] cloud-r8-tool </dt>
      <dd> 
      - TLR / TSR 모니터링 툴 및 알고리즘 검증 및 검수툴 <br />
      - http://gitlab.aimatics.synology.me/cloud-algorithm/cloud-r8-tool <br />
      - 담당자 Ian <br />
      </dd>

      <dt> [Python] KoRoad_VAD_Monitoring_Tool  </dt>
      <dd> 
      - 교통관리공단 VAD 검수툴 <br />
      - ssh://git@gitlab.aimatics.synology.me:30002/api_server/koroad/koroad_vad_monitoring_tool.git <br />
      - 담당자 Ian <br />
      </dd>

      <dt> [Python] R8 Cloud DSM View Tool  </dt>
      <dd> 
      - DSM 이전 버젼, 신규버전 비교 툴 <br />
      - Ian Desktop : C:\Users\Ian\Desktop\R8_true\R8_Cloud_DSM_View_TOOl <br />
      - 담당자 Ian <br />
      </dd>

      <dt> [Python] R8_Utils  </dt>
      <dd> 
      - 슬랙에 올라오는 영상들 날짜 지정해 자동 다운 코드  <br />
      - http://gitlab.aimatics.synology.me/ian_lee/r8_utils <br />
      - 담당자 Ian <br />
      </dd>

    </dl>

    <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
  </Layout>
)

export default NotFoundPage

