import * as React from "react"

import Layout from "../../components/layout"
import Seo from "../../components/seo"

const NotFoundPage = () => (
  <Layout>
    <Seo title="R9 Tools" />
    <h1>R9 Tools</h1>
    <dl>
      <dt> [MFC]  aimatics_faceid_registration_tool</dt>
      <dd> 
      - R9에서 Face ID 등록할 때 사용하는 툴, FTP를 통해 얼굴 정보 저장 <br />
      - ssh://git@gitlab.aimatics.synology.me:30002/r9tools/aimatics_faceid_registrationtool.git <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC]  AlgPerformanceAnalyzerR9 </dt>
      <dd> 
      - R9 rs9 파일을 비교해서 인식 성능 개선 검토에 사용하는 툴, 에이든이 추가 구현하였음 <br />
      - Ted Desktop : D:\Project\2_DSP_CODE\5_R9\003_TOOL\AlgPerformanceAnalyzerR9 <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC] ats-manager-tool </dt>
      <dd> 
      - ATS 시연을 진행할 때 사용하는 관리자용 툴, 원격으로 게임 제어 <br />
      - ssh://git@gitlab.aimatics.synology.me:30002/r9tools/ats-manager-tool.git <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC] default-monitoring-socket </dt>
      <dd> 
      - R9 모니터링툴, 설정값 변경 기능, 기본 브랜치&5ch 시연용 브랜치&ATS 시연용 브랜치 등 포함 <br />
      - ssh://git@gitlab.aimatics.synology.me:30002/r9tools/default-monitoring-socket.git  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC] R9_EventAnalyzer </dt>
      <dd> 
      - R9 mp4 파일의 메타 데이터로부터 인식 결과 재생하거나 rs9파일로 추출 <br />
      - ssh://gituser@115.178.89.182:58/volume1/homes/gituser/R9_EventAnalyzer  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC] vad-monitoring-socket </dt>
      <dd> 
      - R9 VAD 모니터링툴 <br />
      - ssh://git@gitlab.aimatics.synology.me:30002/r9tools/vad-monitoring-socket.git  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC] VideoMapAnalyzer </dt>
      <dd> 
      - 영상 재생하면서 인식 결과 표시하고, GSP 좌표에 맞춰서 지도 업데이트. 카카오 시연용으로 제작. <br />
      - Ted Desktop : D:\Project\2_DSP_CODE\5_R9\003_TOOL\VideoMapAnalyzer  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC] xmlLabelViewer </dt>
      <dd> 
      - xml 파일들을 분석해서 현재 GT 데이터 통계 출력 <br />
      - ssh://gituser@115.178.89.182:58/volume1/homes/gituser/xmlLabelViewer  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> [MFC] VideoLabelTool </dt>
      <dd> 
      - VAD 영상 GT 작업을 위해 제작. 현재는 사용되고 있지 않는듯. <br />
      - Ted Desktop : D:\Project\2_DSP_CODE\4_NVS3320\_tool\VideoLabelTool  <br />
      - 담당자 Ted <br />
      </dd>

      <dt> rs9_GT_convert_tool </dt>
      <dd> 
      - rs9 to png&xml, png&xml to rs9 변환 툴 <br />
      - http://aimatics.synology.me:30000/Nick/rs9_gt_convert_tool  <br />
      - 담당자 Nick <br />
      </dd>

      <dt> rs9_diff_tool </dt>
      <dd> 
      - 원본데이터 / 시뮬레이션 데이터 결과 차이 확인 <br />
      - http://aimatics.synology.me:30000/Aiden/rs9_diff_tool <br />
      - 담당자 Aiden <br />
      </dd>

      <dt> rs9_converter </dt>
      <dd> 
      - RS9 파일 생성을 위한 툴 <br />
      - http://aimatics.synology.me:30000/Aiden/rs9_converter <br />
      - 담당자 Aiden <br />
      </dd>

      <dt> [Python] R9 Paramsite Viewer </dt>
      <dd> 
      - Ian Desktop <br />
      - http://aimatics.synology.me:30000/Aiden/rs9_converter <br />
      - 담당자 Ian <br />
      </dd>

      <dt> [MFC] Monitoring INS </dt>
      <dd> 
      - INS GPS 실시간 라이브 툴 <br />
      - http://gitlab.aimatics.synology.me/TedJung/monitoring-ins <br />
      - 담당자 Ted, Ian <br />
      </dd>

      <dt> [Python] 전시회용 Leaderboard </dt>
      <dd> 
      - American Truck Simulator 연동 R9 시연 프로그램 결과 표기 프로그램 (얼굴 blur, 프린트 등 기능 포함) <br />
      - http://gitlab.aimatics.synology.me/justinjin/leaderboard <br />
      - 담당자 Justin <br />
      </dd>

      <dt> [Python] R9 Database View Tool </dt>
      <dd> 
      - R9 parameter 데이터베이스 view tool <br />
      - http://gitlab.aimatics.synology.me/cloud-algorithm/r9-database-view-tool <br />
      - 담당자 Justin <br />
      </dd>



    </dl>
  </Layout>
)

export default NotFoundPage

