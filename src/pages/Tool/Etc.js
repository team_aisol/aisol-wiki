import * as React from "react"

import Layout from "../../components/layout"
import Seo from "../../components/seo"

const NotFoundPage = () => (
  <Layout>
    <Seo title="ETC Tools" />
    <h1>Tools: Etc</h1>
    <dl>
      <dt> [Python] gps_pos_draw</dt>
      <dd> 
      - AATS DATA (.cts) GPS 위치 지도 그리기 <br />
      - http://aimatics.synology.me:30000/squid-project/gps_pos_draw <br />
      - 담당자 Nick <br />
      </dd>

      <dt> OD_LabelingTool </dt>
      <dd> 
      - OD 용 GT 생성 작업 툴 <br />
      - http://aimatics.synology.me:30000/Nick/labelingtool <br />
      - 담당자 Nick <br />
      </dd>

      <dt> roadmap_data_crawler </dt>
      <dd> 
      - 네이버/카카오/구글 등 로드맵 데이터 크롤링 툴 <br />
      - http://aimatics.synology.me:30000/Nick/roadmap_data_crawler <br />
      - 담당자 Nick <br />
      </dd>

      <dt> xmlLabelRename </dt>
      <dd> 
      - GT xml의 class 이름을 변경하기 위한 툴 <br />
      - Aiden PC <br />
      - 담당자 Aiden <br />
      </dd>

      <dt> xmlLabelViewer </dt>
      <dd> 
      - GT xml의 class 분석 뷰어 툴 <br />
      - Aiden PC <br />
      - 담당자 Aiden <br />
      </dd>

      <dt> xmlLabelSieve </dt>
      <dd> 
      - GT xml의 특정 class만 추출하는 툴 <br />
      - Aiden PC <br />
      - 담당자 Aiden <br />
      </dd>

      <dt> Web_Crawler </dt>
      <dd> 
      - google image에서 keyword 검색 image crawler  <br />
      - Aiden PC / Workstation <br />
      - 담당자 Aiden <br />
      </dd>

      <dt> OSC_data_crawler </dt>
      <dd> 
      - Openstreet cam의 영상 crawler  <br />
      - Workstation <br />
      - 담당자 Aiden <br />
      </dd>

      <dt> [NGK] disp2lidar </dt>
      <dd> 
      - NGK의 lidar데이터 display 툴  <br />
      - Aiden PC <br />
      - 담당자 Aiden <br />
      </dd>

      <dt> [NGK] NGK_TOOL </dt>
      <dd> 
      - NGK의 모니터링 툴  <br />
      - Aiden PC <br />
      - 담당자 Aiden <br />
      </dd>


    </dl>
  </Layout>
)

export default NotFoundPage
