import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"
import { Link } from "gatsby"

import Layout from "../../components/layout"
import Seo from "../../components/seo"
import {Container, Row, Col, Image} from 'react-bootstrap'

const SecondPage = () => (
  <Layout>
    <Seo title="Photo Album" />
    <h1>사진첩</h1><br/>

    <p><font size="+0"><b>삼표 데이터 취득</b></font></p>
    <dl><em><small>
      <dd>- 일산 / 코엑스.</dd>
    </small></em></dl>
    <Container>
    <Row>
      <StaticImage
          src="../../images/random_images/3C_2022_12_08_Sampyo.jpg"
          width={600}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
      </Row>

      <Row>
        <Col>
        <StaticImage
          src="../../images/random_images/3A_2022_12_08_Sampyo.jpg"
          width={600}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>

        <Col>
        <StaticImage
          src="../../images/random_images/3B_2022_12_08_Sampyo.jpg"
          width={600}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>
      </Row>
    </Container>

    <p><font size="+0"><b>Jaden 결혼식 날</b></font></p>
    <dl><em><small>
      <dd>- 결혼 축드립니다!!!!</dd>
    </small></em></dl>
    <Container>
      <Row>
        <Col>
        <StaticImage
          src="../../images/random_images/2A_2022_09_02_Exhibition.jpg"
          width={600}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>
        <Col>
        <StaticImage
          src="../../images/random_images/2B_2022_10_20_Exhibition.jpg"
          width={600}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>
      </Row>
    </Container>

    <p><font size="+0"><b>전시회 날</b></font></p>
    <dl><em><small>
      <dd>- 일산 / 코엑스.</dd>
    </small></em></dl>
    <Container>
      <Row>
        <Col>
        <StaticImage
          src="../../images/random_images/2A_2022_09_02_Exhibition.jpg"
          width={600}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>
        <Col>
        <StaticImage
          src="../../images/random_images/2B_2022_10_20_Exhibition.jpg"
          width={600}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>
      </Row>
    </Container>



    <p><font size="+0"><b>첫 7명 팀 회식!</b></font></p>
    <dl><em><small>
      <dd>- 카일로 택시타고 대구 갈뻔한 날</dd>
    </small></em></dl>
    <Container>
      <Row>
        <Col>
        <StaticImage
          src="../../images/random_images/1A_2022_05_09_team_dinner.jpg"
          width={500}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>

        <Col>
        <StaticImage
          src="../../images/random_images/1D_2022_05_09_team_dinner.jpg"
          width={500}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>
      </Row>

      <Row>
        <Col>
        <StaticImage
          src="../../images/random_images/1B_2022_05_09_team_dinner.jpg"
          width={500}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>

        <Col>
        <StaticImage
          src="../../images/random_images/1C_2022_05_09_team_dinner.jpg"
          width={500}
          quality={95}
          formats={["auto", "webp", "avif"]}
          alt="A Gatsby astronaut"
          style={{ marginBottom: `1.45rem` }}
        />
        </Col>
      </Row>

    </Container>



    <p><font size="+0"><b>닉 전문연구원 끝! 소집해제 회식</b></font></p>
    <dl><em><small>
      <dd>- 이안 테드한테 혼난 날.</dd>
    </small></em></dl>

    <StaticImage
      src="../../images/random_images/0_2021_11_30_team_dinner.jpg"
      width={600}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
    />

    <br/>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default SecondPage
