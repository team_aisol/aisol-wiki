import * as React from "react"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Video from "../../components/video"
import DogVideo from "../../videos/22_11_29_photoism.mp4"

const team_dinner_2022_11_29 = () => (
  <Layout>
    <Seo title="Team Dinner " />
    <h1>Team Dinner</h1>
    {/* <Video
        src="../../videos/22_11_29_photoism.mp4"
        width={800}
        quality={95}  
        videoTitle="Official Music Video on YouTube"
      /> */}

    <p>2022년 11월 29일 2분기 축하 할일이 많아 회식을 진행하였습니다.
      특히 전문연구요원인 저스틴이 훈련소로 가는 마지막날을 위해 모인 뜻깊은 자리였습니다.
      오랜만에 모두가 모인 자리에서 미나리 삼겹에서 회식을 하려 했으나 대기가 많아 1차는 이차돌에서 회식하였습니다.
      인당 최소 하나씩 또는 최대 3개씩 까지 축하일을 하나 나누면서 건배를 하며 단합력을 키웠습니다.
      그리고 2차는 비어킹에서 진행하였는데, 개인적인 얘기가 오고 가던중, 매우 피곤하신 동네 어르신 (저스틴)과 어깨빵 형님인 (카일로)가
      합석하여 매우 재미진 자리가 되었습니다.
      3차는 노래방에서 각자 최소 한곡씩 하고 귀가하던중 남아있던 인원들로 인생네컷에서 포토를 남겼습니다. <br />
      밑에 축하했던일을 목록 리스트 입니다.
    </p>
    <dl>
      <dt> 2022년 11월 29일 2분기 회식 및 단합  </dt>
      <dd> 
      - 테드 본부장 취임 축하 <br />
      - 저스틴 훈련소 입소 축하 <br />
      - 테드 생일 축하 <br />
      - 테드 동생 결혼 축하 <br />
      - 제이든 결혼 축하 <br />
      - 닉 바프 완료 축하 <br />
      - 에이든 소집 해제 축하 <br />
      - 이안 입사 1주년 축하 <br />
      - 카일로 입사 축하 <br />
      - 저스틴 면허 취득 축하 <br /><br />
      </dd>

      <StaticImage
      src="../../images/blog_images/22_11_29_1차_단체2.JPG"
      width={800}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
      />

      <StaticImage
      src="../../images/blog_images/22_11_29_1차_단체3.JPG"
      width={800}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
      />

      <StaticImage
      src="../../images/blog_images/22_11_29_2차_단체1.JPG"
      width={800}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
      />

      <StaticImage
      src="../../images/blog_images/22_11_29_2차_단체2.JPG"
      width={800}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
      />

      <StaticImage
      src="../../images/blog_images/22_11_29_photoisim.JPG"
      width={800}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
      />

    <video controls>
      <source src={DogVideo} type="video/mp4" />
    </video>

    </dl>

  </Layout>
)

export default team_dinner_2022_11_29