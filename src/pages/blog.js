import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"


import Layout from "../components/layout"
import Seo from "../components/seo"

const BlogPage = () => (
  <Layout>
    <Seo title="Blog Page" />
    <h1>Team blog  <br /> <br /> </h1>
    <p>

      <Link className="btn btn-outline-primary" to="/Blog/team_dinner_2022_11_29/"> 2022 11 29 team dinner! </Link> <br /> <br />
      <Link className="btn btn-outline-primary" to="/Blog/photo_album/"> Random 사진첩 </Link> <br /> <br />
      {/* <Link className="btn btn-outline-primary" to="/Tool/R8/"> R8 tools </Link> <br /> <br />
      <Link className="btn btn-outline-primary" to="/Tool/AWS/"> AWS tools </Link> <br /> <br />
      <Link className="btn btn-outline-primary" to="/Tool/Etc/"> Etc tools </Link> <br /> <br /> */}

      <Link className="btn btn-outline-primary" to="/"> Go back to the homepage </Link> <br /> <br />

      {/* <Link to="/Tool/R9/">R9 tools</Link> <br />
      <Link to="/Tool/R8/">R8 tools</Link> <br />
      <Link to="/Tool/AWS/">AWS tools</Link> <br />
      <Link to="/Tool/Etc/">Etc tools</Link> <br />
      <Link to="/">Go back to the homepage</Link> */}
    </p>

  </Layout>
)

export default BlogPage
