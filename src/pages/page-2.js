import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"


import Layout from "../components/layout"
import Seo from "../components/seo"

const SecondPage = () => (
  <Layout>
    <Seo title="Page two" />
    <h1>Our Team</h1>
    <p>Ted</p>
    <dl>
      <dd>- </dd>
    </dl>

    <StaticImage
      src="../images/Jaden1.jpg"
      width={600}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
    />
    <p><font size="+2"><b>[Jaden]</b></font></p>
    <dl><em>
      <dd>- 2013년 05월 입사로, 본명은 이보현이며, 영어이름은 Jaden 입니다.</dd>
      <dd>-현재 담당 업무는 Alg 개발 시험을 담당하고 있습니다.</dd>
      <dd>- 술을 매우 줄겨하고 잘먹습니다. 게임도 좋아 합니다.</dd>
    </em></dl>

    <p><font size="+2"><b>[Nick]</b></font></p>
    <p><font size="+2"><b>[Aiden]</b></font></p>

    <StaticImage
      src="../images/Ian1.jpg"
      width={600}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
    />
    <p><font size="+2"><b>[Ian]</b></font></p>
    <dl><em>
      <dd>- 2021년 10월 입사로, 본명은 이수일이며, 영어이름은 Ian으로 뜻은 하나님의 선물입니다.</dd>
      <dd>- 현재 담당 업무는 AWS Cloud VAD EC2에 적용 입니다</dd>
      <dd>- 루미큐브 진적이 없으며, 테니스를 좋아하며, MBTI는 ISTP입니다</dd>
    </em></dl>

    <p><font size="+2"><b>[Kylo]</b></font></p>
    <p><font size="+2"><b>[Justin]</b></font></p>
    <dl><em>
      <dd>- 2021년 10월 입사로, 본명은 이수일이며, 영어이름은 Ian으로 뜻은 하나님의 선물입니다.</dd>
      <dd>- 현재 담당 업무는 AWS Cloud VAD EC2에 적용 입니다</dd>
      <dd>- 루미큐브 진적이 없으며, 테니스를 좋아하며, MBTI는 ISTP입니다</dd>
    </em></dl>

    <Link to="/">Go back to the homepage</Link>
  </Layout>
)

export default SecondPage
